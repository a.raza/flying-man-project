﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Holds Overall animation methods
/// </summary>
public class BehaviourController : MonoBehaviour
{
    private const string ANIMATION_TRIGGER_VALUE = "Active";

    [Header("Behaviour Controller")]
    [SerializeField] private Animator animator;

    protected delegate void EventEncounter();

    protected virtual void Start() => IsValid();

    private void IsValid()
    {
        if(animator == null)
        {
            Debug.Log("AnimationController Not Operational!");
            this.gameObject.SetActive(false);
            return;
        }
    }

    protected void SetAnimation(Perform _action)
    {
        animator.SetInteger(ANIMATION_TRIGGER_VALUE, (int)_action);
    }

    protected IEnumerator SetAnimation(float wait, EventEncounter executeCommand, Perform _action)
    {
        yield return new WaitForSeconds(wait);
        SetAnimation(_action);
        executeCommand();
    }
}
