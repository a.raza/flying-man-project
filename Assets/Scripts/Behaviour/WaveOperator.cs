﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveOperator : MonoBehaviour
{
	public float WaveSpeed;
	public float WaveHeight;
	public float WavePattern;

	//new mesh
	Mesh _mesh;
	Vector3[] _verts;
	float a_time;

	void Start()
	{
		var mf = GetComponent<MeshFilter>();

		MakeNewMesh(mf);
	}
	// Update is called once per frame
	void Update()
	{
		CalcWave();
	}

	//making new mesh that is writable..
	MeshFilter MakeNewMesh(MeshFilter mf)
	{
		//getting the mesh data and adding into a new mesh
		_mesh = mf.sharedMesh; //sharedmesh is used for reading mesh data

		//creating an array of int,
		//according to the number of triangles in mesh
		var triangles = _mesh.triangles;

		//creating an array of vecter 3,
		//according to the total number of vertix points in the mesh  
		var vertices = new Vector3[triangles.Length];

		//copying all the vertex points in the mesh, into temp var
		var oldVerts = _mesh.vertices;

		//Adding all the vertex points into Vector3 array,
		//and triangles in the mesh, into int array, one by one
		for (var i = 0; i < triangles.Length; i++)
		{
			vertices[i] = oldVerts[triangles[i]];

			triangles[i] = i;
		}

		//assigning the vertices of the new mesh with the vector3 array
		_mesh.vertices = vertices;

		//assigning the triangles of the new mesh with the int array
		_mesh.triangles = triangles;

		//After modifying the vertices..
		//it is often useful to update the normals to reflect the change
		_mesh.RecalculateNormals();

		//getting the vertices to update them in realtime..
		_verts = _mesh.vertices;

		return mf;
	}

	void CalcWave()
	{
		a_time += Time.deltaTime * WaveSpeed;

		for (var i = 0; i < _verts.Length; i++)
		{
			//getting first vertex
			var v = _verts[i];

			// chaing the y axis of the vertex
			v.y = 0.0f;

			var dist = Vector3.Distance(v, Vector3.zero);

			//doing long division to get the final Remainder
			dist = (dist % WavePattern);//WavePattern;

			v.y = Mathf.Sin(a_time * dist) * WaveHeight; // can use Time.time

			//applying the changes to the vertex
			_verts[i] = v;
		}

		//applying the new positions of vertices
		_mesh.vertices = _verts;

		//update the modified mesh
		_mesh.RecalculateNormals();

		//re-writing the mesh data
		GetComponent<MeshFilter>().sharedMesh = _mesh;
	}

}
