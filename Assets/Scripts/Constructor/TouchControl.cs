﻿using UnityEngine;

public class TouchControl
{
    private float deadRadius;

    private bool
        tap = false,
        swipeUp = false,
        swipeDown = false,
        swipeLeft = false,
        swipeRight = false,
        isDraging = false;

    private Vector2
        startTouch,
        swipeDelta;

    public TouchControl(float _deadRadius)
    {
        deadRadius = _deadRadius;
    }

    public void Execut()
    {
        tap = swipeLeft = swipeRight = swipeUp = swipeDown = false;

        #region Standalone Input
        if (Input.GetMouseButtonDown(0))
        {
            tap = true;
            isDraging = true;
            startTouch = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isDraging = false;
            TouchReset();
        }
        #endregion

        #region Mobile Input
        if (Input.touches.Length > 0)
        {
            var touch = Input.touches[0];

            if (touch.phase == TouchPhase.Began)
            {
                isDraging = true;
                tap = true;
                startTouch = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
            {
                isDraging = false;
                TouchReset();
            }
        }
        #endregion

        #region Drag Process
        swipeDelta = Vector2.zero;

        if (isDraging)
        {
            if (Input.touches.Length > 0)
            {
                swipeDelta = Input.touches[0].position - startTouch;
            }
            else if (Input.GetMouseButton(0))
            {
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
            }
        }

        if (swipeDelta.magnitude > deadRadius)
        {
            float
                x = swipeDelta.x,
                y = swipeDelta.y;

            //Debug.Log("deadRadius");

            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                //LEFT or Right
                if (x < 0)
                {
                    swipeLeft = true;
                }
                else
                {
                    swipeRight = true;
                }
            }
            //else
            //{
            //    //UP or DOWN
            //    if (y < 0)
            //    {
            //        swipeDown = true;
            //    }
            //    else
            //    {
            //        swipeUp = true;
            //    }
            //}

            TouchReset();
        }
        #endregion
    }

    private void TouchReset()
    {
        startTouch = swipeDelta = Vector2.zero;
        isDraging = false;
    }

    public Vector2 SwipeDelta
    {
        get { return swipeDelta; }
    }

    public bool Tap
    {
        get { return tap; }
    }

    public bool SwipeLeft
    {
        get { return swipeLeft; }
    }

    public bool SwipeRight
    {
        get { return swipeRight; }
    }

    public bool SwipeDown
    {
        get { return swipeDown; }
    }

    public bool SwipeUp
    {
        get { return swipeUp; }
    }
}
