﻿using UnityEngine;

public class CameraOperator : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private float smoothSpeed;
    [SerializeField] private Vector3 offset;

    private void Start() => IsValid();
    private void FixedUpdate() => Operate();

    private void IsValid()
    {
        if(target == null)
        {
            Debug.LogError("CameraOperator Not Operational!");
            this.gameObject.SetActive(false);
            return;
        }
    }

    private void Operate()
    {
        var desiredPosition = target.position + offset;
        transform.position = new Vector3(desiredPosition.x, desiredPosition.y, transform.position.z);
        //var smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        //transform.position = smoothedPosition;
    }
}
