﻿using System.Collections;
using UnityEngine;

/// <summary>
/// Following class provides touch interface controlls
/// </summary>
public class ControlsOperator : FlyingManOperator
{
    private TouchControl touchControl;

    private const float
        DEAD_RADIUS = 125f,     
        CENTER_VALUE = 0f,
        SWIPE_VALUE = 1.5f,
        RIGHT_SIDE = 1.5f,
        LEFT_SIDE = -1.5f;

    private bool isAcitve = false;

    protected override void Start()
    {
        base.Start();

        touchControl = new TouchControl(DEAD_RADIUS);

        StartCoroutine(OnStartFlying());
    }

    protected override void Update()
    {
        base.Update();

        if (isAcitve == true)
        {
            OnUpdate();
        }
    }

    private IEnumerator OnStartFlying()
    {
        yield return new WaitForSeconds(3.5f);

        var position = transform.position;
        transform.position = new Vector3(position.x, position.y, CENTER_VALUE);

        isAcitve = true;
    }

    private void OnUpdate()
    {
        touchControl.Execut();

        if (touchControl.SwipeRight == true)
        {
            //Debug.Log("swipeRight");

            var position = transform.position;

            if (position.z < RIGHT_SIDE)
            {
                transform.position = new Vector3(position.x, position.y, position.z + SWIPE_VALUE);
            }
        }

        if (touchControl.SwipeLeft == true)
        {
            //Debug.Log("swipeLeft");

            var position = transform.position;

            if (position.z > LEFT_SIDE)
            {
                transform.position = new Vector3(position.x, position.y, position.z - SWIPE_VALUE);
            }
        }
    }
}