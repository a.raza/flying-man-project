﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public class EnemyGenerator : LevelGenerator
{
    private const string ENEMY_AREA = "EnemyPoint";
    private const float Y_AXIS = 1.11f;

    [Header("Enemy Generator")]
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private int amountOfEnemies;

    private GameObject[] enemyPoints;

    protected override void Start()
    {
        base.Start();

        GenerateEnemies();
    }

    private void GenerateEnemies()
    {
        enemyPoints = GameObject.FindGameObjectsWithTag(ENEMY_AREA);
        var max = enemyPoints.Length;

        Debug.Log(string.Format("Available Areas {0}", max));

        if(amountOfEnemies > max)
        {
            amountOfEnemies = max;
        }

        System.Random rnd = new System.Random();
        enemyPoints = enemyPoints.OrderBy(x => rnd.Next()).ToArray();

        for (int index = 0; index < amountOfEnemies; index++)
        {
            var enemy = Instantiate(enemyPrefab);
            var point = enemyPoints[index].transform.position;
            var creationPoint = new Vector3(point.x, Y_AXIS, point.z);
            enemy.transform.position = creationPoint;
        }
    }
}
