﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    // LEVEL GENERATOR
    // flyOverPerFloor = 3 seconds (approximately)
    // time = 30 seconds (the level should last around this value)
    // floors = time / flyOverPerFloor
    // floors = 10 (amount of floors that needs to be generated)

    private const float
        FLYOVER_PER_FLOOR = 3,
        GAME_TIME = 30,
        X_AXIS = 33f,
        Y_AXIS = -1.75f,
        Z_AXIS = 0f;

    private const int
        OPPOSITE_ROTATION = 180;

    private const string 
        BOOSTER = "AltitudeTrigger";

    [Header("Level Generator")]
    [SerializeField] private int numberOfSmokersActive;
    [SerializeField] private GameObject[] floorsStructures;
    [SerializeField] private GameObject finalFloorStructure;
    [SerializeField] private Transform environmentHolder;

    private GameObject[] smokers;

    private int previousFloor = 0;

    protected virtual void Start() => GenerateLevel();

    private void GenerateLevel()
    {
        int
            floorsCount = (int)(GAME_TIME / FLYOVER_PER_FLOOR),
            structuresLength = floorsStructures.Length;

        var space = X_AXIS;

        #region Create 1st Floor With Smoke
        int rotation = 0;
        var floor = Instantiate(floorsStructures[1]).transform;
        FloorPlacement(floor, rotation, space);
        space -= X_AXIS;
        #endregion

        #region Create Rest Of Floors
        floorsCount -= 1;
        for (int index = 1; index < floorsCount; index++)
        {
            var floorIndex = Random.Range(0, structuresLength);

            floor = Instantiate(floorsStructures[floorIndex == previousFloor ? 0 : floorIndex]).transform;
            FloorPlacement(floor, rotation == 0 ? 1 : 0, space);
            previousFloor = floorIndex;
            space -= X_AXIS;
        }
        #endregion

        #region Active Limited Number Of Smoke
        ActiveSmokers();
        #endregion

        #region Create Second Last Floor With Smoke
        floor = Instantiate(floorsStructures[0]).transform;
        FloorPlacement(floor, 1, space);
        space -= X_AXIS;
        #endregion

        #region Create Final Floor 
        var finalFloor = Instantiate(finalFloorStructure).transform;
        FloorPlacement(finalFloor, 0, space);
        #endregion
    }

    /// <summary>
    /// Following will place the floors accordingly
    /// </summary>
    /// <param name="floor"></param>
    /// <param name="rotation"></param>
    /// <param name="spaceBetween"></param>
    private void FloorPlacement(Transform floor, int rotation, float spaceBetween)
    {
        floor.SetParent(environmentHolder);
        floor.localScale = Vector3.one;
        floor.localRotation = Quaternion.Euler(0, rotation == 1 ? OPPOSITE_ROTATION : 0, 0);
        floor.localPosition = new Vector3(spaceBetween, Y_AXIS, Z_AXIS);
    }

    /// <summary>
    /// Will Active certain amount of smoke area for glider
    /// </summary>
    private void ActiveSmokers()
    {
        smokers = GameObject.FindGameObjectsWithTag(BOOSTER);
        var max = smokers.Length;

        Debug.Log(string.Format("Available Smokes {0}", max));

        smokers = SortCollection(smokers);

        numberOfSmokersActive = (numberOfSmokersActive > max) ? max : numberOfSmokersActive;

        for(int index = numberOfSmokersActive; index < max; index++)
        {
            smokers[index].gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Will sort smoke areas in a manner that will make sure the glider can manage altitude
    /// </summary>
    /// <returns></returns>
    protected GameObject[] SortCollection(GameObject[] _objects)
    {
        List<GameObject> newCollection = new List<GameObject>();
        var max = _objects.Length;

        for (int index = 0; index < max; index += 2)
        {
            if (index > max)
                break;

            newCollection.Add(_objects[index]);
        }

        foreach (var _obj in _objects)
        {
            if (!newCollection.Contains(_obj))
                newCollection.Add(_obj);
        }

        return newCollection.ToArray();
    }

    private void OnApplicationQuit()
    {
        // The game should last between 30 to 40 seconds since it is hyper casual
        Debug.Log(string.Format("TIME: {0}", Time.time));
    }
}
