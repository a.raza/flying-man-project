﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// Following class will switch the character from Agent to user-control
/// </summary>
public class ControlsSwitch : BehaviourController
{
    private const float
        END_POINT = 0.16f;

    protected const float
        ROTATION_Y = -90,
        ROTATION_X = -25;

    [Header("Agent Controller")]
    [SerializeField] private Transform target;
    [SerializeField] protected NavMeshAgent agent;
    [SerializeField] protected Rigidbody body;

    [Header("Altitude Operator")]
    [SerializeField] protected ForceMode mode;
    [SerializeField] protected float speed, speedDeduction;
    protected float force, speedBoost, normalSpeed;

    protected bool isControllerActive = false;

    protected override void Start() => IsValid();

    private void IsValid()
    {
        base.Start();

        if (target == null || agent == null || body == null)
        {
            Debug.LogError(string.Format("ControlsSwitch Not Operational!"));
            this.gameObject.SetActive(false);
            return;
        }

        WalkTowardsJumpPoint();
        StartCoroutine(Operate());
    }

    /// <summary>
    /// Walk Towards Jump Point
    /// </summary>
    private void WalkTowardsJumpPoint()
    {
        SetAnimation(Perform.MOVE);
        agent.SetDestination(target.position);
    }

    /// <summary>
    /// Do Jump When Near
    /// </summary>
    /// <returns></returns>
    IEnumerator Operate()
    {
        yield return new WaitForSeconds(Time.fixedDeltaTime);

        var currentPoint = target.position;
        var endPoint = agent.transform.position;
        var distance = Vector3.Distance(endPoint, currentPoint);

        if (distance < END_POINT)
        {
            agent.isStopped = true;
            agent.enabled = false;
            StartCoroutine(RotationOperator());
            SetAnimation(Perform.JUMP);
        }
        else
        {
            StartCoroutine(Operate());
        }
    }

    protected IEnumerator RotationOperator()
    {
        yield return new WaitForSeconds(Time.fixedDeltaTime);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, ROTATION_Y, 0), force * Time.fixedDeltaTime);
        StartCoroutine(RotationOperator());        
    }

    public void ActiveControls()
    {
        isControllerActive = true;
    }
}
