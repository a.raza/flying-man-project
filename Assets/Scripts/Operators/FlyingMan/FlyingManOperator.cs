﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// This class holds the overall interactions of the character with the elements in the level
/// </summary>
public class FlyingManOperator : ControlsSwitch
{
    private const string
        BOOSTER = "Steam",
        SUCCESS = "Success",
        FAIL = "Enemy";

    private const float SPEED = 100;

    protected override void Start() => OnActive();
    protected virtual void Update() => ManageAltitude();

    [SerializeField] private UnityEvent onSuccess;
    [SerializeField] private UnityEvent onFail;

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == BOOSTER)
        {
            Debug.LogWarning("GainAltitude");
            GainAltitude();
        }

        if(collision.tag == SUCCESS)
        {
            onSuccess?.Invoke();
        }

        if (collision.tag == FAIL)
        {
            onFail?.Invoke();
        }
    }

    private void OnActive()
    {
        base.Start();

        AdjustRigidBody();
        AdjustSpeed();
    }

    private void AdjustRigidBody()
    {
        body.velocity = Vector3.zero;
        body.angularVelocity = Vector3.zero;
    }

    private void AdjustSpeed()
    {
        force = speed;
        speed *= SPEED;
        normalSpeed = speed;
        speedBoost = speed * force;
    }

    private void ManageAltitude()
    {
        if (isControllerActive == true)
            body.AddForce(transform.forward * (speed * Time.smoothDeltaTime), mode);
    }

    private void GainAltitude()
    {
        transform.rotation = Quaternion.Euler(new Vector3(ROTATION_X, ROTATION_Y, 0));
        speed = speedBoost;
        body.useGravity = false;
        StartCoroutine(SpeedNormalizer());
    }

    private IEnumerator SpeedNormalizer()
    {
        yield return new WaitForSeconds(0.125f);

        Debug.Log("SpeedNormalizer");

        if (speed > normalSpeed)
        {
            var deduction = speed / speedDeduction;

            speed -= deduction;

            StartCoroutine(SpeedNormalizer());
        }
        else
        {
            body.useGravity = true;
            speed = normalSpeed;
        }
    }
}
