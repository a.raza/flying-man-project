﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : UIOperator
{
    [Header("Game Manager")]
    [SerializeField] private GameObject display;
    [SerializeField] private TextMeshProUGUI title;

    protected override void Start()
    {
        base.Start();
    }

    public void ActiveDisplay(string state)
    {
        display.SetActive(true);

        switch (state)
        {
            case "pause":
                OnPauseState();
                break;
            case "fail":
                OnFailState();
                break;
            case "success":
                OnSuccessState();
                break;
        }

        Time.timeScale = 0;
    }

    private void OnPauseState()
    {
        title.text = "PAUSED";
        pause.gameObject.SetActive(false);
        resume.gameObject.SetActive(true);
        tryAgain.gameObject.SetActive(true);
    }

    private void OnFailState()
    {
        title.text = "You Failed!";
        pause.gameObject.SetActive(false);
        resume.gameObject.SetActive(false);
        tryAgain.gameObject.SetActive(true);
    }

    private void OnSuccessState()
    {
        title.text = "Success!";
        pause.gameObject.SetActive(false);
        resume.gameObject.SetActive(false);
        tryAgain.gameObject.SetActive(true);
    }
}
