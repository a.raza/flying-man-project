﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathFindingOperator : BehaviourController
{
    [SerializeField] private float idleTime;
    [SerializeField] private List<Transform> targets = new List<Transform>();
    [SerializeField] private NavMeshAgent agent;

    private int currentEndPoint = 0, limit = 0;
    private bool wait = false;

    protected override void Start()
    {
        base.Start();
        IsValid();

        SetAnimation(wait == true ? Perform.IDLE : Perform.MOVE);
    }

    private void Update() => Operate();

    private void IsValid()
    {
        if(targets.Count <= 0 || agent == null)
        {
            Debug.LogError(string.Format("PathFindingOperator Not Operational!"));
            this.gameObject.SetActive(false);
            return;
        }

        limit = targets.Count;
    }

    private void Operate()
    {
        //Debug.Log(wait);

        var currentPoint = targets[currentEndPoint].position;
        var agentPosition = agent.transform.position;
        var endPoint = Vector3.Distance(agentPosition, currentPoint);

        if (endPoint <= 0.5f)
        {
            SetAnimation(Perform.IDLE);

            wait = true;

            currentEndPoint++;

            if(currentEndPoint >= limit)
            {
                currentEndPoint = 0;
            }

            currentPoint = targets[currentEndPoint].position;

            StartCoroutine(SetAnimation
                (
                    idleTime,
                        () =>
                        {
                            wait = false;
                            transform.rotation = Quaternion.LookRotation(agent.velocity.normalized * 100);
                        },
                    Perform.MOVE)
                );
        }

        if (wait == false)
            agent.SetDestination(currentPoint);
    }
}
