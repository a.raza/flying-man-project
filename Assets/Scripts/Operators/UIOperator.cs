﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIOperator : MonoBehaviour
{
    [Header("UI Operator")]
    [SerializeField] protected Button pause;
    [SerializeField] protected Button resume;
    [SerializeField] protected Button tryAgain;

    protected virtual void Start()
    {
        PrepareUI();
    }

    private void PrepareUI()
    {
        pause.onClick.AddListener(() => { OnPause(); });
        resume.onClick.AddListener(() => { OnResume(); });
        tryAgain.onClick.AddListener(() =>
        {
            OnResume();
            OnRetry();
        });
    }

    private void OnPause()
    {
        Time.timeScale = 0;
    }

    private void OnResume()
    {
        Time.timeScale = 1;
    }

    private void OnRetry()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
