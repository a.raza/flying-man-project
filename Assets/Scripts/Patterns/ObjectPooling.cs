﻿using System;
using UnityEngine;

public class ObjectPooling
{
    private GameObject[] collection;
    public delegate void onCreate(GameObject obj);

    /// <summary>
    /// This will generate n number of prefabs as according to the amount
    /// </summary>
    /// <param name="amount"></param>
    /// <param name="prefab"></param>
    public ObjectPooling(int amount, GameObject prefab, onCreate create)
    {
        collection = new GameObject[amount];

        for (int i = 0; i < amount ; i++)
        {
            collection[i] = UnityEngine.Object.Instantiate(prefab);
            collection[i].SetActive(false);
            create(collection[i]);
        }
    }

    /// <summary>
    /// This will generate n number of prefabs, adding each prefab in the collection
    /// </summary>
    /// <param name="amount"></param>
    /// <param name="prefabs"></param>
    public ObjectPooling(int amount, GameObject[] prefabs, onCreate create)
    {
        var prefabsAmount = prefabs.Length;
        var n = 0;

        if(prefabsAmount > amount)
        {
            amount = prefabsAmount;
        }

        collection = new GameObject[amount];

        for (int i = 0; i < amount; i++)
        {
            if (n >= prefabsAmount)
            {
                n = 0;
            }

            if(prefabs[n] != null)
            {
                collection[i] = UnityEngine.Object.Instantiate(prefabs[n]);
                collection[i].SetActive(false);
                create(collection[i]);
                n++;
            }
        }
    }

    public GameObject GetAvailableObject()
    {
        try
        {
            Debug.Log("ObjectPooling GetAvailableObject");
            return Array.Find(collection, o => !o.activeInHierarchy);
        }
        catch
        {
            return null;
        }
    }
}
